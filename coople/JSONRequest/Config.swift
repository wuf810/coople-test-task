//
//  Config.swift
//  coople
//
//  Created by Michael Kaye on 30/09/2019.
//  Copyright © 2019 Michael Kaye. All rights reserved.
//

import Foundation

struct Config {
  
    // Source URL
    static let sourceURL = "http://www.coople.com/resources/api/work-assignments/public-jobs/list?pageNum=0&pageSize=200"

}
