//
//  Job.swift
//  coople
//
//  Created by Michael Kaye on 30/09/2019.
//  Copyright © 2019 Michael Kaye. All rights reserved.
//

import Foundation

// JSON Response
/*
 
 data: {
     items: [
         {
             datePublished:1569834090000,
             periodFrom:1571569200000,
             jobLocation:-{
                 extraAddress:"",
                 addressStreet:"",
                 zip:"HA9",
                 city:"Wembley",
                 countryId:78
             },
             hourlyWage: {
                amount:8.210000000000001,
                currencyId:3
             },
             workAssignmentId:"a036395d-c0cc-4f87-9070-86bb14929447",
             salary: {
                 amount:49.26,
                 currencyId:3
             },
             jobSkill: {
                jobProfileId:4
             },
             salaryWithHolidayPay: {
                 amount:55.21,
                 currencyId:3
             },
             branchLink:"https://coople.app.link/VDnBp4TG9U",
             workAssignmentName:"Doughnut Seller at a stadium",
             waReadableId:"60190024062",
             hourlyWageWithHolidayPay: {
                 amount:9.199999999999999,
                 currencyId:3
            }
        },
    ],
 total:141
 }
 
 */

// MARK: Decodable
struct Assignment : Decodable {
    
    let assignment : String
    let street : String
    let postcode : String
    let city : String
    
    enum CodingKeys: String, CodingKey {
        case assignment = "workAssignmentName"
        case location = "jobLocation"
        case street = "addressStreet"
        case postcode = "zip"
        case city = "city"
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        assignment = try container.decode(String.self, forKey: .assignment)
        let location = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .location)
        street = try location.decode(String.self, forKey: .street)
        postcode = try location.decode(String.self, forKey: .postcode)
        city = try location.decode(String.self, forKey: .city)
    }
    
}

struct Assignments : Decodable {
    
    let httpStatus : Int
    let numberOfJobs : Int
    let jobs: [Assignment]
    
    enum CodingKeys: String, CodingKey {
        case response = "data"
        case jobs = "items"
        case numberOfJobs = "total"
        case httpStatus = "status"
    }

    init(from decoder: Decoder) throws {
        let responseContainer = try decoder.container(keyedBy: CodingKeys.self)
        httpStatus = try responseContainer.decode(Int.self, forKey: .httpStatus)
        
        // If we have anything other than a 200, then throw an error
        guard httpStatus == 200 else {
            throw ResponseError.HTTPError
        }
        
        let jobsContainer = try responseContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .response)
        numberOfJobs = try jobsContainer.decode(Int.self, forKey: .numberOfJobs)
        jobs = try jobsContainer.decode([Assignment].self, forKey: .jobs)
    }
    
}

// MARK: Basic Error Handler
enum ResponseError: Error {
    case HTTPError
}
