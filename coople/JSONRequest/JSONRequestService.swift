//
//  JSONRequestService.swift
//  coople
//
//  Created by Michael Kaye on 30/09/2019.
//  Copyright © 2019 Michael Kaye. All rights reserved.
//

import Foundation

class JSONRequestService {
    
    func getJobs(completion: @escaping ([Job]) -> () ) {
        
        if let url = URL(string: Config.sourceURL) {
            URLSession.shared.dataTask(with: url) { data, response, error in
                
                var jobs = [Job]()
                
                if let data = data {
                    do {
                        let assignments = try JSONDecoder().decode(Assignments.self, from: data)
                        
                        /*
                         Whilst we could potentially use an array of Assignment struct objects
                         directly for our dataSource model, my preference here is to
                         keep them distinct and for a single purpose.
                         
                         So let's loop through the Assignments and create an array of Jobs objects to return
                         */
                        
                        for assignment in assignments.jobs {
                            jobs.append(Job(assigment: assignment.assignment, street: assignment.street, postcode: assignment.postcode, city: assignment.city))
                        }
                        
                    } catch let error {
                        print(error)
                    }
                }
                
                completion(jobs)
                
                }.resume()
        }
        
    }
    
}
