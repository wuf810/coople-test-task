//
//  Job.swift
//  coople
//
//  Created by Michael Kaye on 30/09/2019.
//  Copyright © 2019 Michael Kaye. All rights reserved.
//

import Foundation

class Job : CustomStringConvertible {
    
    // MARK: Properties
    
    var assignment : String?
    var street : String?
    var postcode : String?
    var city : String?

    // MARK: Initializer
    
    init(assigment : String, street : String, postcode : String, city : String) {
        self.assignment = assigment
        self.street = street
        self.postcode = postcode
        self.city = city
    }
    
    // MARK: StringConvertibles
    
    /// CustomStringConvertible
    public var description : String {
        
        let descriptionArray : [[String : String]] = [
            ["assignment"   : assignment ?? ""],
            ["street"       : street ?? ""],
            ["postcode"     : postcode ?? ""],
            ["city"         : city ?? ""],
        ]
        
        let description = descriptionArray.map { (dict) -> String in
            return "\(dict.keys.first!) : \(dict.values.first!)"
            }.joined(separator: "\n")
        
        return description
        
    }
    
    /// CustomDebugStringConvertible
    public var debugDescription : String {
        return description
    }

    
}
