//
//  ViewController.swift
//  coople
//
//  Created by Michael Kaye on 29/09/2019.
//  Copyright © 2019 Michael Kaye. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Hide NavigationBar Separator
        let navigationBar = navigationController!.navigationBar
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()

    }


}

