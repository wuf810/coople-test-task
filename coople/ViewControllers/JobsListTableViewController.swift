//
//  JobsListTableViewController.swift
//  coople
//
//  Created by Michael Kaye on 29/09/2019.
//  Copyright © 2019 Michael Kaye. All rights reserved.
//

import UIKit

class JobsListTableViewController: UITableViewController {
   
    let cellReuseIdentifier = "JobCellReuseIdentifier"
    
    var jobs = [Job]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup our ActivityIndicatorView
        activityIndicator()
        
        // Add a Pull To Refresh because it's good UI/UX
        refreshControl()
        
        // Setup TableView
        tableViewSetup()
        
        // Get Jobs
        getJobs()
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jobs.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! JobTableViewCell

        let job = jobs[indexPath.row]

        cell.assignment.text = job.assignment
        cell.street.text = job.street
        cell.postcode.text = job.postcode
        cell.city.text = job.city

        return cell
    }

    // MARK: Data Loading
    
    @objc func refreshData() {
        getJobs()
        refreshControl?.endRefreshing()
    }
    
    func getJobs() {
        
        // Load our Jobs
        activityIndicatorView.startAnimating()
        
        let requestService = JSONRequestService()
        
        requestService.getJobs { (jobs) in
            
            self.jobs = jobs
            
            // Reload our Table now we have our data
            DispatchQueue.main.async {
                self.activityIndicatorView.stopAnimating()
                self.tableView.reloadData()
            }
            
        }

    }
    
    // MARK: TableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        do {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }

    // MARK: TableView Setup
    func tableViewSetup() {
        /*
         Hide Separators when displaying empty cells
         i.e. on initial display of the tableView before loading the jobs
         */
        self.tableView.tableFooterView = UIView()
    }
    
    // MARK: UIRefreshControl Setup
    func refreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(refreshData), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    // MARK: UIActivityIndicatorView Setup
    var activityIndicatorView = UIActivityIndicatorView()
    
    func activityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        activityIndicatorView.style = UIActivityIndicatorView.Style.whiteLarge
        
        /* Let's shift the activity view up so that's it's centered in the tableView
        i.e. we need to offset by the height of the status and Navigation Bar
        */
        
        let centre = self.view.center
        activityIndicatorView.center = CGPoint(x: centre.x, y: centre.y - self.topbarHeight)
        self.view.addSubview(activityIndicatorView)
    }
}

extension UIViewController {
        var topbarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
}
