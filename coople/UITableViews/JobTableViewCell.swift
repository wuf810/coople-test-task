//
//  JobTableViewCell.swift
//  coople
//
//  Created by Michael Kaye on 30/09/2019.
//  Copyright © 2019 Michael Kaye. All rights reserved.
//

import UIKit

class JobTableViewCell: UITableViewCell {

    @IBOutlet weak var assignment: UILabel!
    @IBOutlet weak var street: UILabel!
    @IBOutlet weak var postcode: UILabel!
    @IBOutlet weak var city: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
