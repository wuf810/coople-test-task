# Coople Test Task

### **Task Details:**

The requirements for the app are defined here:

[https://paper.dropbox.com/doc/Coople-Test-Task--AjXmKcRbF2NFudGkQ6veyiW0AQ-Q06s6Re57DdOhME9xx9hf]()

### **Overview**

* Author : Michael Kaye
* Date : 30th September 2019

### **Requirements**:

* Xcode 10.3
* Swift 4.2 +
* iOS 10.0 +

### **What this demo includes**:

* Native request of, and native parsing of a request's JSON response
	*  NB Although it is possible to use third parties libraries like Alamofire for the networking stack and SwiftyJSON for parsing the JSON response, in this demo the amounts of data are so small, it made sense to use iOS native commands such as SessionURL to retrieve the data, and the Swift 4 Decodable Protocol for the decoding the response.
* The JSON response is decoded into an array of ```Job``` model objects.
* These jobs are displayed using a standard UITableViewController using a custom UITableViewCell class (```JobTableViewCell```).
* The UITableViewController is styled to present a more functional and pleasant user interface :
	*  The background colour is set to the "Red" used in the current Coople app. In addition this colour matches the colour used in Splashscreen and main view, creating a theme flowing through the app.
	*  Text colour is set to white to make text clear and easier readable. In addition, in the job listing, the postcode of the job is presented in bold as this is potentially one of the most important pieces of information for a user.
	*  "Pull to refresh" control added to the control
	*  A "Loading" indicator is displayed whilst the data is be retrieved & decoded and hidden once displayed.
* Use of Autolayout has been made to display the response data in each cell. The ```workAssignmentName``` (as does the cell) dynamically sizes to ensure that the whole title is visible.
* App Icon "borrowed" from the current Coople app and remains copyright of Coople.
* Coople logo "borrowed" from the Coople website and remains copyright of Coople.

### **What’s missing from this demo**:

* Error handling for handling any invalid JSON i.e. if an error is thrown when decoding the JSON response.
* The hiding of "jobs" with incomplete data. For example if the “job name” is missing, it may be sensible to not show this particular job in the results. This would present a much better user experience for the user.
* Any UI to display error information i.e. No network available
* Paging and caching. For larger sets of results it would make sense to use paging and/or caching mechanisms for improved scrolling performance